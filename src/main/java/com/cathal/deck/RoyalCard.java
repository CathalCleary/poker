package com.cathal.deck;

/**
 * @author Cathal
 * @since 2018
 * 
 * Defines a card with royal value
 */
public class RoyalCard extends Card{
	private Suit suit;
	private Rank rank;
	private int number;
	
	public RoyalCard(final Suit suit, final Rank rank){
		this.suit = suit;
		this.rank = rank;
		this.number = calcValue(rank);
	}
	
	public int getNumber() {
		return number;
	}
	
	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}

	private int calcValue(final Rank rank){
		switch(rank){
			case Ace:
				return 14;
			case King:
				return 13;
			case Queen:
				return 12;
			default:
				return 11;
		}
	}
	
}
