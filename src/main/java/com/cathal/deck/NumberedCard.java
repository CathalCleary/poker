package com.cathal.deck;

/**
 * @author Cathal
 * @since 2018
 * 
 * Defines a card with numerical values
 */
public class NumberedCard extends Card{
	private Suit suit;
	private int number;
	
	public NumberedCard(final Suit suit, final int number){
		this.number = number;
		this.suit = suit;
	}
	
	public int getNumber() {
		return number;
	}

	public Suit getSuit() {
		return suit;
	}

}
