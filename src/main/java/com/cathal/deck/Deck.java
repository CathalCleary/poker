package com.cathal.deck;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Cathal
 * @since 2018
 * 
 * Responsible for building deck, managing deck and dealing hands
 *
 */
public class Deck implements DeckInterface{
	
	private static Map<Rank, Integer> mapCardRankToValue = new HashMap<>();
	private static DeckInterface deckInstance = null;
	private int deckSize = 51;
	
	private Deck(){
	}
	
	public static DeckInterface getInstance(){
		if(deckInstance == null){
			deckInstance = new Deck();
		}
		
		return deckInstance;
	}
	
	static{
		mapCardRankToValue.put(Rank.Jack, 11);
		mapCardRankToValue.put(Rank.Queen, 12);
		mapCardRankToValue.put(Rank.King, 13);
		mapCardRankToValue.put(Rank.Ace, 14);
	}
	
	private List<Card> deck = new ArrayList<>();
	
	/**
	 * {@inheritDoc}
	 */
	public List<Card> getDeck(){
		return deck;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void constructDeck(){
		deck.removeAll(deck);
		for(int i = 0; i < 4; i++){
			for(int j = 1; j < 14; j++){
				if(j == 1){
					deck.add(new RoyalCard(Suit.values()[i], Rank.Ace));
				}else if(j == 11){
					deck.add(new RoyalCard(Suit.values()[i], Rank.Jack));
				}else if(j == 12){
					deck.add(new RoyalCard(Suit.values()[i], Rank.Queen));
				}else if(j == 13){
					deck.add(new RoyalCard(Suit.values()[i], Rank.King));
				}else{
					deck.add(new NumberedCard(Suit.values()[i], j));
				}
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Card[] deal(){
		Card[] hand = new Card[5];
		for(int i = 0; i < 5; i++){
			hand[i] = getRandomCard();
		}
		return hand;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Card getRandomCard()
	{
	   deckSize = deck.size() - 1;
	   Card topDecked = null;
	   int index = 0;
	   while(topDecked == null){
		   int range = deckSize + 1;    
		   index = (int)(Math.random() * range);
		   topDecked = deck.get(index);
		   
	   }
	   deck.remove(index);
	   return topDecked;
	}
	
}
