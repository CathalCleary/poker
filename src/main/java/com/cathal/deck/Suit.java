package com.cathal.deck;

/**
 * @author Cathal
 * @since 2018
 * 
 * An enum that lists the suits contained in a deck of cards
 *
 */
public enum Suit {
	Clubs(0),
	Diamonds(1),
	Hearts(2),
	Spades(3);
	
	Suit(int suitDirectoryReference){
	}
}
