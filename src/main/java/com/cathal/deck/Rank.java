package com.cathal.deck;

/**
 * @author Cathal
 * @since 2018
 * 
 * An Enum that lists the royal ranks in a deck of cards
 *
 */
public enum Rank {
	Ace,
	Jack,
	Queen,
	King
}
