package com.cathal.deck;

import java.util.List;

/**
* @author Cathal
* @since 2018
* 
* Defines methods for building deck
*
*/
public interface DeckInterface {
	
	/**
	 * Creates a hand of 5 random cards taken from the {@code Deck}
	 * @return Card array containing 5 random cards.
	 */
	public Card[] deal();
	
	/**
	 * Retrieves a random card from the deck
	 * @return A random {@link Card}
	 */
	public Card getRandomCard();
	
	/**
	 * Builds a deck of 52 playing cards
	 */
	public void constructDeck();

	/**
	 * Returns a deck of cards
	 * @return A {@link List} of {@link Card}s
	 */
	public List<Card> getDeck();
}
