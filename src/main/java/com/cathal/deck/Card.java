package com.cathal.deck;


//TODO: Split these classes into a class heirarchy: abstract Card, NumericCard, RoyalCard

/**
 * 
 * @author Cathal
 * @since 2018
 * 
 * Constructs a Card
 * A Card contains either a number and a suit or a rank (Ace, King, Queen, Jack) and a suit
 */
public abstract class Card {
	public abstract int getNumber();
	public abstract Suit getSuit();
}
