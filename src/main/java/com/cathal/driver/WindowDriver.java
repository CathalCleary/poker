package com.cathal.driver;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.RoyalCard;
import com.cathal.game.Game;
import com.cathal.player.Human;
import com.cathal.winCalculator.WinCalculator;

public class WindowDriver extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JButton playAgainBtn;
    private JButton submitBetBtn;
    private JButton discardCardBtn;
    private JButton foldBtn;
    private JTextArea playerCardTextArea;
    private JCheckBox card1;
    private JCheckBox card2;
    private JCheckBox card3;
    private JCheckBox card4;
    private JCheckBox card5;
    private JTextArea see;
    private JTextArea raise;
    private JLabel betLabel;
    private JTextArea computerCardTextArea;
    private JLabel computerHandLabel;
    private JLabel playerHandLabel;
    private JLabel cardDiscardLabel;
    private JTextArea userNameField;
    private JTextField statusField;
    private JLabel statusLabel;
    private JLabel userNameLabel;
    private final DeckInterface deck = Deck.getInstance();
    private Human player = null;
    private Human computer = null;
    private boolean cardsDiscarded = false;

    public WindowDriver() {
        playAgainBtn = new JButton ("Play Again?");
        submitBetBtn = new JButton ("Bet");
        discardCardBtn = new JButton ("Discard Cards");
        foldBtn = new JButton ("Fold");
        playerCardTextArea = new JTextArea (5, 5);
        card1 = new JCheckBox ("");
        card2 = new JCheckBox ("");
        card3 = new JCheckBox ("");
        card4 = new JCheckBox ("");
        card5 = new JCheckBox ("");
        see = new JTextArea (5, 5);
        raise = new JTextArea (5, 5);
        betLabel = new JLabel ("See/Raise");
        computerCardTextArea = new JTextArea (5, 5);
        computerHandLabel = new JLabel ("Computers Hand");
        playerHandLabel = new JLabel ("Your Hand");
        cardDiscardLabel = new JLabel ("Cards To Discard");
        userNameField = new JTextArea (5, 5);
        statusField = new JTextField (5);
        statusLabel = new JLabel ("Status");
        userNameLabel = new JLabel ("UserName");
        

        setPreferredSize (new Dimension (784, 523));
        setLayout (null);
        
        add (playAgainBtn);
        add (submitBetBtn);
        add (discardCardBtn);
        add (foldBtn);
        add (playerCardTextArea);
        add (card1);
        add (card2);
        add (card3);
        add (card4);
        add (card5);
        add (see);
        add (raise);
        add (betLabel);
        add (computerCardTextArea);
        add (computerHandLabel);
        add (playerHandLabel);
        add (cardDiscardLabel);
        add (userNameField);
        add (statusField);
        add (statusLabel);
        add (userNameLabel);

        playAgainBtn.setBounds (560, 475, 100, 20);
        submitBetBtn.setBounds (215, 330, 100, 20);
        discardCardBtn.setBounds (450, 325, 140, 20);
        foldBtn.setBounds (440, 475, 100, 25);
        playerCardTextArea.setBounds (115, 120, 485, 95);
        card1.setBounds (125, 235, 100, 25);
        card2.setBounds (275, 235, 100, 25);
        card3.setBounds (335, 235, 100, 25);
        card4.setBounds (450, 235, 100, 25);
        card5.setBounds (560, 235, 60, 25);
        see.setBounds (225, 290, 45, 25);
        raise.setBounds (280, 290, 50, 25);
        betLabel.setBounds (115, 290, 100, 25);
        computerCardTextArea.setBounds (115, 15, 485, 95);
        computerHandLabel.setBounds (610, 40, 100, 25);
        playerHandLabel.setBounds (605, 150, 100, 25);
        cardDiscardLabel.setBounds (630, 235, 100, 25);
        userNameField.setBounds (460, 380, 180, 80);
        statusField.setBounds (45, 370, 260, 140);
        statusLabel.setBounds (40, 340, 100, 25);
        userNameLabel.setBounds (645, 380, 100, 25);
        
        playAgainBtn.addActionListener(this);
        submitBetBtn.addActionListener(this);
        discardCardBtn.addActionListener(this);
        foldBtn.addActionListener(this);
        
        raise.setEditable(false);
        discardCardBtn.setEnabled(false);
        foldBtn.setEnabled(false);
        
        deck.constructDeck();
    }


    public static void main (String[] args) {
        JFrame frame = new JFrame ("MyPanel");
        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add (new WindowDriver());
        frame.pack();
        frame.setVisible (true);
    }


	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		Object eventTarget = actionEvent.getSource();

		if(eventTarget == playAgainBtn){
			foldBtn.setEnabled(true);
			playAgainBtn.setEnabled(false);
			cardsDiscarded = false;
			computerCardTextArea.setText("");
			playerCardTextArea.setText("");
			raise.setEditable(false);
			discardCardBtn.setEnabled(false);
			submitBetBtn.setEnabled(true);
		   if(getUserNameFromGui()){
			player = new Human(deck, userNameField.getText(), 1000);
			computer = new Human(deck, "Computer", 1000);
			statusField.setText("Cards Dealt....Place Bet");
			
			playerCardTextArea.setText(convertHandToString(player.getHand()));
			foldBtn.setEnabled(true);
		   }
		}
		
		if(eventTarget == foldBtn){
			foldBtn.setEnabled(false);
			playAgainBtn.setEnabled(true);
			player.fold();
			statusField.setText(WinCalculator.calculateWinner(player, computer) + " wins!");
		}
		
		if(eventTarget == submitBetBtn){
			if(!raise.isEditable()){
				player.bet(Integer.valueOf(see.getText()));
			}else if(Integer.valueOf(see.getText()) < Game.raisedAmount){
				statusField.setText("You must see the raised amount");
			}else{
				try{
					player.seeAndRaise(Integer.valueOf(see.getText()), Integer.valueOf(raise.getText()));
				}catch(NumberFormatException nfe){
					player.seeAndRaise(Integer.valueOf(see.getText()), 0);
				}
				discardCardBtn.setEnabled(true);
			}
			
			if(computerHasRaised()){
				statusField.setText("Computer has raised Pot is "+ Game.potAmount + " Amount raised is " + Game.raisedAmount + "\n credits left " + player.getCredits());
				raise.setEditable(true);
			}else{
				if(!cardsDiscarded){
					statusField.setText("Choose cards to discard");
					submitBetBtn.setEnabled(false);
				}else{
					raise.setEditable(false);
					computerCardTextArea.setText(convertHandToString(computer.getHand()));
					statusField.setText(WinCalculator.calculateWinner(player, computer) + " wins!");
					playAgainBtn.setEnabled(true);
				}
			}
			
		}
		
		if(eventTarget == discardCardBtn){
			final List<Card> cardsToDiscard = new ArrayList<>();
			if(card1.isSelected()){
				cardsToDiscard.add(player.getHand()[0]);
			}
			
			if(card2.isSelected()){
				cardsToDiscard.add(player.getHand()[1]);
			}
			
			if(card3.isSelected()){
				cardsToDiscard.add(player.getHand()[2]);
			}
			
			if(card4.isSelected()){
				cardsToDiscard.add(player.getHand()[3]);
			}
			
			if(card5.isSelected()){
				cardsToDiscard.add(player.getHand()[4]);
			}
			player.discardCards(cardsToDiscard);
			discardCardBtn.setEnabled(false);
			playerCardTextArea.setText(convertHandToString(player.getHand()));
			submitBetBtn.setEnabled(true);
			raise.setEditable(false);
			cardsDiscarded = true;
		}
	}
	
	private String convertHandToString(final Card[] hand){
		final StringBuilder sb = new StringBuilder();
		for(Card card: hand){
			if(card instanceof RoyalCard){
				RoyalCard royalCard = (RoyalCard) card;
				sb.append(royalCard.getRank() + " " + royalCard.getSuit() + ", ");
			}else{
				sb.append(card.getNumber() + " " + card.getSuit() + ", ");
			}
		}
		return sb.toString();
	}


	private boolean getUserNameFromGui() {
		final String userName = userNameField.getText();
		String message = "You must enter a username!";
		if(userName == null || userName.equals("")){
			statusField.setText(message);
			return false;
		}
		
		return true;
	}
	
	//Temporary static AI until a more sophisticated one is put in place
		private boolean computerHasRaised(){
			computer.seeAndRaise(Game.raisedAmount, Game.raisedAmount);
			
			if(Game.raisedAmount > 0){
				return true;
			}
			return false;
		}
}