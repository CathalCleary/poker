package com.cathal.driver;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.RoyalCard;
import com.cathal.game.Game;
import com.cathal.player.Human;
import com.cathal.winCalculator.WinCalculator;

/**
 * A temporary driver class to run the poker game logic
 * This will be removed once a more sophisticated front end is put in place
 * @author Cathal
 * @since 2019
 *
 */
public class CommandLineDriver {
	
	private static Scanner scanner = new Scanner(System.in);;
	private static DeckInterface deck = Deck.getInstance();

	public static boolean playAgain(){
		System.out.println("Play Poker? y/n");
		boolean playAgain = true;
		
		while(playAgain){
			switch(scanner.nextLine()){
			case "y":
				return true;
			case "n":
				return false;
			default:
				errorMessage();
			}
		}
		return playAgain;
	}
	
	private static void errorMessage() {
		System.out.println("Invalid input - Enter y (for yes) or n (for no)");
		
	}
	
	//Temporary static AI until a more sophisticated one is put in place
	private static void tempAI(final Human player1, final Human player2){
		promptForBet(player1);
		player2.seeAndRaise(Game.raisedAmount, Game.raisedAmount);
		
		if(Game.raisedAmount > 0){
			System.out.println("Pot: " + Game.potAmount);
			System.out.println("Computer has seen and raised " + Game.raisedAmount);
			promptForRaise(player1);
			System.out.println("Pot: " + Game.potAmount);
			System.out.println("You have seen and raised " + Game.raisedAmount);
		}
		player2.seeAndRaise(Game.raisedAmount, 0);
	}

	//Rough to test flow of logic for now...need to add some sort of AI for computer player for proper flow
	public static void startGame(){
		deck.constructDeck();
		while(playAgain()){
			Human player1 = new Human(deck, enterId(), 1000);
			Human player2 = new Human(deck, "Computer", 1000);
			
			printPlayerHand("Your hand", player1.getHand());
			
			tempAI(player1, player2);
			
			player1.discardCards(discardHand(player1.getHand()));
			
			printPlayerHand("Your hand", player1.getHand());
			
			tempAI(player1, player2);
			printPlayerHand("Your hand", player1.getHand());
			printPlayerHand("Computer hand", player2.getHand());
			
			System.out.println("Winner: " + WinCalculator.calculateWinner(player1, player2));
			System.out.println();
		}
	}
	
	private static List<Card> discardHand(Card[] hand) {
		System.out.println("Provide index for cards to swap");
		List<Card> cardsToDiscard = new ArrayList<>();
		boolean finished = false;
		int numCardsSwapped = 0;
		
		while(!finished && numCardsSwapped < 5){
			System.out.print("Swap another card? y/n	");
			final String input = scanner.next();
			switch(input){
			case "y":
				numCardsSwapped = swapCards(hand, cardsToDiscard,
						numCardsSwapped);
				break;
			case "n":
				finished = true;
				break;
			default:
				errorMessage();
			}
		}
		return cardsToDiscard;
		
	}

	private static int swapCards(Card[] hand, List<Card> cardsToDiscard,
			int numCardsSwapped) {
		System.out.println("Enter card to swap (0,1,2,3,4)");
		final int index = scanner.nextInt();
		if(index < 0 || index > 4){
			System.out.println("Invalid index");
		}else{
			cardsToDiscard.add(hand[index]);
			numCardsSwapped++;
		}
		return numCardsSwapped;
	}

	private static void printPlayerHand(final String message, Card[] hand) {
		System.out.println(message);
		System.out.println();
		for(Card card: hand){
			if(card instanceof RoyalCard){
				RoyalCard royalCard = (RoyalCard) card;
				System.out.println(royalCard.getRank() + " " + royalCard.getSuit());
			}else{
				System.out.println(card.getNumber() + " " + card.getSuit());
			}
		}
		
	}

	private static void promptForRaise(Human player) {
		System.out.println("Enter amount to see and raise");
		int amountToSee = scanner.nextInt();
		int amountToRaise = scanner.nextInt();
		player.seeAndRaise(amountToSee, amountToRaise);
		
	}
	
	private static String enterId() {
		System.out.print("Enter id:	");
		return scanner.nextLine();
	}
	
	private static void promptForBet(final Human player) {
		System.out.println("Do you want to raise? y/n");
		boolean validInput = false;
		
		while(!validInput){
			switch(scanner.nextLine()){
			case "y":
				validInput = true;
				player.bet(bet());
				break;
			case "n":
				validInput = true;
				break;
			default:
				errorMessage();
			}
		}
		
	}

	private static int bet() {
		int betAmount = 0;
				
		while(betAmount <= 0){
			System.out.print("How much do you want to bet?: ");
			betAmount = scanner.nextInt();
		}
		return betAmount;
		
	}

	
	public static void main(String[] args) {
		startGame();
	}

}
