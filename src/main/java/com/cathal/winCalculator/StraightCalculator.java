package com.cathal.winCalculator;

import com.cathal.player.PlayerInterface;

class StraightCalculator {
	
	static boolean checkForStraight(PlayerInterface player) {
		int currentCard = player.getHand()[0].getNumber();
		
		for(int i = 1; i < player.getHand().length; i++){
			final int nextCard = player.getHand()[i].getNumber();
			
			if(Math.abs(currentCard - nextCard) != 1){
				return false;
			}
			currentCard = nextCard;
		}
		return true;
	}

}
