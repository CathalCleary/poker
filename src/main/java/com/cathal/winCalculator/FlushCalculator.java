package com.cathal.winCalculator;

import com.cathal.deck.Suit;
import com.cathal.player.PlayerInterface;

class FlushCalculator {
	
	static boolean checkForFlush(final PlayerInterface player){
		int numberOfMatchingSuitsInHand = 0;
		
		final Suit card = player.getHand()[0].getSuit();
			
		for(int i = 1; i < player.getHand().length; i++){
			if(player.getHand()[i].getSuit() == card){
				numberOfMatchingSuitsInHand++;
			}
		}
		return numberOfMatchingSuitsInHand == 4;
	}

}
