package com.cathal.winCalculator;

import com.cathal.player.PlayerInterface;

/**
 * @author Cathal
 * @since 2019
 * 
 * Calculates the winner of the poker game
 */
public class WinCalculator {
	
	/**
	 * Calculates the winner of a poker game
	 * @param player1 A poker player
	 * @param player2 A poker player
	 * @return The winner of the game or a message to inform of a draw
	 */
	public static String calculateWinner(final PlayerInterface player1, final PlayerInterface player2) {
		
		player1.returnCardsToDeck();
		player2.returnCardsToDeck();
		
		if(player1.isFolded()){
			return player2.getId();
		}else if(player2.isFolded()){
			player1.getId();
		}
		
		calculateScore(player1);
		calculateScore(player2);
		
		if(player1.getScore() == player2.getScore()){
			if(player1.getHighCard().getNumber() == player2.getHighCard().getNumber()){
				HighCardCalculator.calculateHighCard(null, player1);
				HighCardCalculator.calculateHighCard(null, player2);
			}
		}
		
		if(player1.getScore() > player2.getScore()){
			return player1.getId();
		}
		
		if(player2.getScore() > player1.getScore()){
			return player2.getId();
		}
		
		return HighCardCalculator.compareHighCards(player1, player2);
	}
	
	public static void calculateScore(final PlayerInterface player){
		boolean isAStraight = StraightCalculator.checkForStraight(player);
		boolean isAFlush = FlushCalculator.checkForFlush(player);
		
		if(isAStraight && isAFlush){
			HighCardCalculator.calculateHighCard(null, player);
			player.setScore(9);
		}else if (isAStraight){
			HighCardCalculator.calculateHighCard(null, player);
			player.setScore(4);
		}else if (isAFlush){
			HighCardCalculator.calculateHighCard(null, player);
			player.setScore(5);
		}else{
			HighCardCalculator.calculateHighCard(null, player);
			MatchingCardsCalculator.checkForMatchingCards(player);
		}
	}

}
