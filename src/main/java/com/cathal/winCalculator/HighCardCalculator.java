package com.cathal.winCalculator;

import com.cathal.deck.Card;
import com.cathal.player.PlayerInterface;

class HighCardCalculator {
	
	private static String DRAW = "Draw";
	
	static void calculateHighCard(final Card prevHighCard, final PlayerInterface player){
		Card highestCard = player.getHand()[0];
		
		for(int i = 0; i < player.getHand().length; i++){
			if(player.getHand()[i].getNumber() > highestCard.getNumber()){
				if(prevHighCard == null || prevHighCard.getNumber() != player.getHand()[i].getNumber()){
					highestCard = player.getHand()[i];
				}
			}
		}
		player.setHighCard(highestCard);
	}
	
	static String compareHighCards(final PlayerInterface player1, final PlayerInterface player2){
		int countDownToDraw = 5;
		while(player1.getHighCard().getNumber() == player2.getHighCard().getNumber() && countDownToDraw > 0){
			calculateHighCard(player1.getHighCard(), player1);
			calculateHighCard(player2.getHighCard(), player2);
			countDownToDraw--;
		}
		if(player1.getHighCard().getNumber() > player2.getHighCard().getNumber()){
			return player1.getId();
		}else if(player2.getHighCard().getNumber() > player1.getHighCard().getNumber()){
			return player2.getId();
		}
		return DRAW;
	}

}
