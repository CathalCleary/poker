package com.cathal.winCalculator;

import com.cathal.deck.Card;
import com.cathal.player.PlayerInterface;

class MatchingCardsCalculator {
	
	static void checkForMatchingCards(final PlayerInterface player){
		int score = 0;
	
		for(int i = 0; i < player.getHand().length;){
			final Card currentCard = player.getHand()[i];
			final int cardNumber = currentCard.getNumber();
			for(int j = i+1; j < player.getHand().length; j++){
				if(player.getHand()[j].getNumber() == cardNumber){
					player.setHighCard(currentCard);
					score++;
				}
				
			}
			i++;
		}
		if(score >= 4){
			score+=2;
		}
		player.setScore(score);
	}

}
