package com.cathal.game;



/**
 * 
 * @author Cathal
 * @since 2019
 * 
 * Defines the control fields for a Poker game
 *
 */
public class Game {
	public static int potAmount = 0;
	public static boolean roundOver = false;
	public static int raisedAmount = 0;
}
