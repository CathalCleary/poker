package com.cathal.player;

import com.cathal.deck.DeckInterface;
import com.cathal.winCalculator.WinCalculator;

/**
 * 
 * @author Cathal
 * @since 2019
 * 
 * Builds, sorts and stores a computers hand
 * Stores the players highest value card for stalemate cases.
 */
public class Computer extends Player implements PlayerInterface, ComputerInterface{
	
	public Computer(final DeckInterface deck, final int credits){
		super(deck, "Computer", credits);
	}

	@Override
	public String decideNextMove() {
		WinCalculator.calculateScore(this);
		
		if(credits >= 900 && score == 1){
			return "see";
		}
		
		if(credits >= 700 && checkForLowHands()){
			return "raise";
		}
		
		if(credits >= 600 && checkForLowHands()){
			return "see";
		}
		
		if(credits >= 300 && checkForHighHands()){
			return "raise";
		}
		
		if(credits < 300 && checkForHighHands()){
			return "see";
		}
		
		if(credits > 0 && score == 9){
			return "raise";
		}
		
		return "fold";
	}

	private boolean checkForHighHands() {
		return score == 4 || score == 5 || score == 6 || score == 8;
	}

	private boolean checkForLowHands() {
		return score == 2 || score == 3;
	}

}
