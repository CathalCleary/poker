package com.cathal.player;

/**
 * 
 * @author Cathal
 * Defines methods used by a Computer player
 */
public interface ComputerInterface extends PlayerInterface {
	public String decideNextMove();
}
