package com.cathal.player;

import java.util.List;

import com.cathal.deck.Card;

/**
 * @author Cathal
 * @since 2018
 * 
 * Builds, sorts and stores a players hand
 * Stores the players highest value card for stalemate cases.
 */
public interface PlayerInterface {
	
	public String getId();
	
	public Card[] getHand();
	
	public void setHighCard(final Card highCard);

	public Card getHighCard();
	
	public void setScore(final int score);
	
	public int getScore();
	
	public int getCredits();
	
	public boolean isFolded();
	
	public void bet(final int betAmount);
	
	public void seeAndRaise(final int seeAmount, final int raiseAmount);
	
	public void fold();
	
	public void returnCardsToDeck();
	
	public void discardCards(List<Card> cardsToDiscard);

}
