package com.cathal.player;

import java.util.List;

import com.cathal.deck.Card;
import com.cathal.deck.DeckInterface;
import com.cathal.game.Game;

/**
 * @author Cathal
 * @since 2018
 * 
 * Builds, sorts and stores a players hand
 * Stores the players highest value card for stalemate cases.
 */
public abstract class Player implements PlayerInterface {
	private final DeckInterface deck;
	private final String id;
	private final Card[] hand;
	private Card highCard;
	protected int score;
	protected int credits;
	private boolean folded = false;
	
	public Player(final DeckInterface deck, final String id, final int credits) {
		this.deck = deck;
		this.id = id;
		this.hand = deck.deal();
		this.credits = credits;
		sortHand();
	}
	
	public String getId(){
		return id;
	}
	
	public Card[] getHand() {
		return hand;
	}
	
	public void setHighCard(final Card highCard){
		this.highCard = highCard;
	}

	public Card getHighCard() {
		return highCard;
	}
	
	public void setScore(final int score){
		this.score = score;
	}
	
	public int getScore(){
		return score;
	}
	
	public int getCredits(){
		return credits;
	}
	
	public boolean isFolded() {
		return folded;
	}
	
	private void sortHand(){
		Card swapper;

		for(int i = 0; i < hand.length - 1; i++){
			for(int j = 0; j < hand.length - 1 - i; j++){
				if(hand[j].getNumber() > hand[j + 1].getNumber()){
					swapper = hand[j];
					hand[j] = hand[j + 1];
					hand[j + 1] = swapper;
				}
			}
		}
	}
	
	public void bet(final int betAmount){
		credits -= betAmount;
		Game.raisedAmount += betAmount;
		Game.potAmount += betAmount;
	}
	
	public void seeAndRaise(final int seeAmount, final int raiseAmount){
		bet(seeAmount + raiseAmount);
		Game.raisedAmount = raiseAmount;
		if(seeAmount < raiseAmount){
			fold();
		}
		if(raiseAmount == 0){
			endRound();
		}
	}
	
	public void fold(){
		endRound();
		folded = true;
	}

	private void endRound() {
		Game.roundOver = true;
	}

	public void discardCards(List<Card> cardsToDiscard) {
		for(Card card: cardsToDiscard){
			for(int i = 0; i < hand.length; i++){
				if(hand[i] == null){
					continue;
				}
				if(card.getNumber() ==  hand[i].getNumber() && card.getSuit() == hand[i].getSuit()){
					Card cardToReturnToDeck = hand[i];
					hand[i] = deck.getRandomCard();
					returnCardsToDeck(cardToReturnToDeck);
				}
			}
		}
		
	}
	
	public void returnCardsToDeck(){
		for(Card card: hand){
			returnCardsToDeck(card);
		}
	}
	
	private void returnCardsToDeck(Card card){
		if(deck.getDeck().size() < 52){
			deck.getDeck().add(card);
		}
	}
	
	
}
