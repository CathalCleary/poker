package com.cathal.player;

import com.cathal.deck.DeckInterface;

/**
 * 
 * @author Cathal
 * @since 2019
 * 
 * Builds, sorts and stores a human players hand
 * Stores the players highest value card for stalemate cases.
 */
public class Human extends Player implements PlayerInterface {
	
	public Human(final DeckInterface deck, final String id, final int credits){
		super(deck, id, credits);
	}
}
