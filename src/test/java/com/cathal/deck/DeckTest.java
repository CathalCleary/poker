package com.cathal.deck;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;

public class DeckTest {
	
	DeckInterface deck = Deck.getInstance();
	
	@Before
	public void setUp(){
		deck.constructDeck();
	}
	
	@Test
	public void whenConstructDeckIsCalled_thenAHandIsBeBuilt_andCardsDealtAreRemovedFromDeck() {
		final Card [] hand = deck.deal();
		//Only checking length here, since the values are random
		assertEquals(47, deck.getDeck().size());
		assertEquals(5, hand.length);
	}
	
	@Test
	public void whenACardIsDrawn_aCardIsReturned(){
		Card card = null;
		assertNull(card);
		card = deck.getRandomCard();
		assertNotNull(card);
	}
}
