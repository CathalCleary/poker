package com.cathal.winCalculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Rank;
import com.cathal.deck.RoyalCard;
import com.cathal.deck.Suit;
import com.cathal.player.Human;
import com.cathal.player.PlayerInterface;
import com.cathal.winCalculator.WinCalculator;

public class WinCalculatorStraightTest {
	
	private static final String DRAW = "Draw";
	private static final String PLAYER1 = "player1";
	private static final String PLAYER2 = "player2";
	
	DeckInterface deck = Mockito.mock(DeckInterface.class);

	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasJunk_thenPlayerOneWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 1);
		hand2[2] = new NumberedCard(Suit.Diamonds, 2);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.King);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoAPair_thenPlayerOneWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 3);
		hand2[2] = new NumberedCard(Suit.Diamonds, 2);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.King);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwohasThreeOfAKind_thenPlayerOneWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 3);
		hand2[2] = new NumberedCard(Suit.Spades, 3);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.King);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasAFlush_thenPlayerTwoWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 6);
		hand2[1] = new NumberedCard(Suit.Clubs, 5);
		hand2[2] = new NumberedCard(Suit.Clubs, 4);
		hand2[3] = new RoyalCard(Suit.Clubs, Rank.King);
		hand2[4] = new RoyalCard(Suit.Clubs,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasAFullHouse_thenPlayerTwoWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 3);
		hand2[2] = new NumberedCard(Suit.Spades, 3);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.King);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.King);
		
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasFourOfAKind_thenPlayerTwoWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 3);
		hand2[2] = new NumberedCard(Suit.Spades, 3);
		hand2[3] = new NumberedCard(Suit.Hearts, 3);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.King);
		
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasAStraight_thenThePlayerWithTheHighestStraight(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 3);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Hearts, 5);
		hand2[3] = new NumberedCard(Suit.Diamonds, 6);
		hand2[4] = new NumberedCard(Suit.Spades, 7);
		
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenPlayerOneHasAStraight_andPlayerTwoHasAStraight_andBothStraightsAreEqual_thenTheGameIsADraw(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 2);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 5);
		hand1[4] = new NumberedCard(Suit.Spades, 6);
		
		hand2[0] = new NumberedCard(Suit.Hearts, 2);
		hand2[1] = new NumberedCard(Suit.Clubs, 3);
		hand2[2] = new NumberedCard(Suit.Diamonds, 4);
		hand2[3] = new NumberedCard(Suit.Clubs, 5);
		hand2[4] = new NumberedCard(Suit.Hearts, 6);
		
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(DRAW, WinCalculator.calculateWinner(player1, player2));
	}

}
