package com.cathal.winCalculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Rank;
import com.cathal.deck.RoyalCard;
import com.cathal.deck.Suit;
import com.cathal.player.Human;
import com.cathal.player.PlayerInterface;
import com.cathal.winCalculator.WinCalculator;

public class WinCalculatorStraightFlushTest {

	private static final String DRAW = "Draw";
	private static final String PLAYER1 = "player1";
	private static final String PLAYER2 = "player2";
	
	DeckInterface deck = Mockito.mock(DeckInterface.class);
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasJunk_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 5);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 7);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 3);
		hand2[4] = new NumberedCard(Suit.Spades, 6);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAPair_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 5);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 7);
		hand2[1] = new NumberedCard(Suit.Diamonds, 7);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 3);
		hand2[4] = new NumberedCard(Suit.Spades, 6);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasTwoPairs_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 5);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 7);
		hand2[1] = new NumberedCard(Suit.Diamonds, 7);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 5);
		hand2[4] = new NumberedCard(Suit.Spades, 6);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasThreeOfAKind_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 5);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 7);
		hand2[1] = new NumberedCard(Suit.Diamonds, 7);
		hand2[2] = new NumberedCard(Suit.Spades, 7);
		hand2[3] = new NumberedCard(Suit.Hearts, 5);
		hand2[4] = new NumberedCard(Suit.Spades, 6);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAStraight_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 5);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 8);
		hand2[1] = new NumberedCard(Suit.Diamonds, 9);
		hand2[2] = new NumberedCard(Suit.Spades, 10);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.Jack);
		hand2[4] = new RoyalCard(Suit.Spades, Rank.Queen);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAFlush_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 5);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 3);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Clubs, 9);
		hand2[2] = new NumberedCard(Suit.Clubs, 10);
		hand2[3] = new RoyalCard(Suit.Clubs, Rank.Jack);
		hand2[4] = new RoyalCard(Suit.Clubs, Rank.Queen);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAFullHouse_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 5);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 3);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Clubs, 2);
		hand2[2] = new NumberedCard(Suit.Clubs, 2);
		hand2[3] = new RoyalCard(Suit.Clubs, Rank.Queen);
		hand2[4] = new RoyalCard(Suit.Clubs, Rank.Queen);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasFourOfAKind_thenPlayerOneWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 5);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 3);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Clubs, 2);
		hand2[2] = new NumberedCard(Suit.Clubs, 2);
		hand2[3] = new NumberedCard(Suit.Clubs, 2);
		hand2[4] = new RoyalCard(Suit.Clubs, Rank.Queen);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAStraightFlush_thenPlayerWithHigherFlushWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 5);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 3);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Hearts, 2);
		hand2[1] = new NumberedCard(Suit.Hearts, 3);
		hand2[2] = new NumberedCard(Suit.Hearts, 4);
		hand2[3] = new NumberedCard(Suit.Hearts, 5);
		hand2[4] = new NumberedCard(Suit.Hearts, 6);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenPlayerOneHasAStraightFlush_andPlayerTwoHasAStraightFlush_andNeitherPlayerHasAHigherFlush_thenThenTheGameIsADraw() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 5);
		hand1[1] = new NumberedCard(Suit.Clubs, 4);
		hand1[2] = new NumberedCard(Suit.Clubs, 3);
		hand1[3] = new NumberedCard(Suit.Clubs, 6);
		hand1[4] = new NumberedCard(Suit.Clubs, 7);
		
		hand2[0] = new NumberedCard(Suit.Hearts, 5);
		hand2[1] = new NumberedCard(Suit.Hearts, 4);
		hand2[2] = new NumberedCard(Suit.Hearts, 3);
		hand2[3] = new NumberedCard(Suit.Hearts, 6);
		hand2[4] = new NumberedCard(Suit.Hearts, 7);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(DRAW, WinCalculator.calculateWinner(player1, player2));
		
	}

}
