package com.cathal.winCalculator;

import static org.junit.Assert.*;

import org.junit.Test;

import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.player.Human;
import com.cathal.player.PlayerInterface;

public class WinCalculatorTest {
	
	private DeckInterface deck = Deck.getInstance();

	@Test
	public void whenAWinnerIsCalculated_thenThePlayersCardsAreReturnedToTheDeck() {
		deck.constructDeck();
		PlayerInterface player1 = new Human(deck, "Player1", 1000);
		PlayerInterface player2 = new Human(deck, "Player2", 1000);
		
		assertNotNull(player1.getHand()[0]);
		assertNotNull(player2.getHand()[0]);
		
		assertEquals(42, deck.getDeck().size());
		
		WinCalculator.calculateWinner(player1, player2);
		
		assertEquals(52, deck.getDeck().size());
	}

}
