package com.cathal.winCalculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Rank;
import com.cathal.deck.RoyalCard;
import com.cathal.deck.Suit;
import com.cathal.player.Human;
import com.cathal.player.PlayerInterface;

public class WinCalculatorForfeitTest {

	@Test
	public void whenPlayerOneFolds_thePlayerTwoWins() {
		final DeckInterface deck = Mockito.mock(Deck.class);
		deck.constructDeck();

		Card hand1[] = new Card[5];
		hand1[0] = new NumberedCard(Suit.Clubs, 3);
		hand1[1] = new NumberedCard(Suit.Diamonds, 3);
		hand1[2] = new NumberedCard(Suit.Hearts, 4);
		hand1[3] = new NumberedCard(Suit.Diamonds, 4);
		hand1[4] = new NumberedCard(Suit.Spades, 5);
		
		Card hand2[] = new Card[5];
		hand2[0] = new NumberedCard(Suit.Clubs, 1);
		hand2[1] = new NumberedCard(Suit.Diamonds, 2);
		hand2[2] = new NumberedCard(Suit.Diamonds, 3);
		hand2[3] = new NumberedCard(Suit.Hearts, 4);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.King);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, "player1", 1000);
		PlayerInterface player2 = new Human(deck, "player2", 1000);
		
		player1.fold();
		assertEquals("player2", WinCalculator.calculateWinner(player1, player2));
	}

}
