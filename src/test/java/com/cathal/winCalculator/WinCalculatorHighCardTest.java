package com.cathal.winCalculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Rank;
import com.cathal.deck.RoyalCard;
import com.cathal.deck.Suit;
import com.cathal.player.Human;
import com.cathal.player.PlayerInterface;
import com.cathal.winCalculator.WinCalculator;

public class WinCalculatorHighCardTest {
	
	private static final String PLAYER1 = "player1";
	private static final String PLAYER2 = "player2";
	
	DeckInterface deck = Mockito.mock(DeckInterface.class);

	@Test
	public void whenNeitherPlayerHasAWinningHand_andAllCardsAreNumerical_thenThePlayerWithTheHighestValueCardWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new NumberedCard(Suit.Diamonds, 3);
		hand1[3] = new NumberedCard(Suit.Hearts, 7);
		hand1[4] = new NumberedCard(Suit.Spades, 9);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 10);
		hand2[4] = new NumberedCard(Suit.Spades, 8);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenNeitherPlayerHasAWinningHand_andAllCardsAreNumerical_AndNoPlayerHasAHighestCard_thenThePlayerWithTheNextHighestCardWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new NumberedCard(Suit.Diamonds, 3);
		hand1[3] = new NumberedCard(Suit.Hearts, 7);
		hand1[4] = new NumberedCard(Suit.Spades, 9);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 9);
		hand2[4] = new NumberedCard(Suit.Spades, 8);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenNeitherPlayerHasAWinningHand_andOnePlayersHasARoyalCard_thenThePlayerWithTheHighestValueCardWins() {
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new NumberedCard(Suit.Diamonds, 3);
		hand1[3] = new NumberedCard(Suit.Hearts, 7);
		hand1[4] = new RoyalCard(Suit.Spades, Rank.Queen);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new NumberedCard(Suit.Hearts, 10);
		hand2[4] = new NumberedCard(Suit.Spades, 8);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
		
	}
	
	@Test
	public void whenNeitherPlayerHasAWinningHand_andBothPlayersHaveARoyalCard_thenThePlayerWithTheHighestValueCardWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new NumberedCard(Suit.Diamonds, 3);
		hand1[3] = new NumberedCard(Suit.Hearts, 7);
		hand1[4] = new RoyalCard(Suit.Spades, Rank.Queen);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.Ace);
		hand2[4] = new NumberedCard(Suit.Spades,8);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenNeitherPlayerHasAWinningHand_andBothPlayersHaveMultipleRoyalCards_thenThePlayerWithTheHighestValueCardWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new RoyalCard(Suit.Diamonds, Rank.Jack);
		hand1[3] = new RoyalCard(Suit.Hearts, Rank.Queen);
		hand1[4] = new RoyalCard(Suit.Spades, Rank.King);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.Ace);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER2, WinCalculator.calculateWinner(player1, player2));
	}
	
	@Test
	public void whenNeitherPlayerHasAWinningHand_andBothPlayersHaveMultipleRoyalCards_AndNoPlayerHasAHighestCard_thenThePlayerWithTheNextHighestCardWins(){
		Card[] hand1 = new Card[5];
		Card[] hand2 = new Card[5];
		
		hand1[0] = new NumberedCard(Suit.Clubs, 1);
		hand1[1] = new NumberedCard(Suit.Diamonds, 2);
		hand1[2] = new RoyalCard(Suit.Diamonds, Rank.Jack);
		hand1[3] = new RoyalCard(Suit.Hearts, Rank.Queen);
		hand1[4] = new RoyalCard(Suit.Spades, Rank.King);
		
		hand2[0] = new NumberedCard(Suit.Clubs, 2);
		hand2[1] = new NumberedCard(Suit.Diamonds, 4);
		hand2[2] = new NumberedCard(Suit.Diamonds, 5);
		hand2[3] = new RoyalCard(Suit.Hearts, Rank.King);
		hand2[4] = new RoyalCard(Suit.Spades,Rank.Jack);
		
		Mockito.when(deck.deal()).thenReturn(hand1).thenReturn(hand2);
		
		PlayerInterface player1 = new Human(deck, PLAYER1, 0);
		PlayerInterface player2 = new Human(deck, PLAYER2, 0);
		
		assertEquals(PLAYER1, WinCalculator.calculateWinner(player1, player2));
	}
}
