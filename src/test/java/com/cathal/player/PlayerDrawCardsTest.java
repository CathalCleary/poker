package com.cathal.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;

public class PlayerDrawCardsTest {
	private final static DeckInterface DECK = Deck.getInstance();
	private static final String PLAYER_ID1 = "player1";
	private static final int CREDITS = 1000;
	private static List<Card> deck;
	
	@Before
	public void setUpBeforeClass(){
		DECK.constructDeck();
		deck = DECK.getDeck();
	}
	
	@Test
	public void whenPlayerSelectsOneCardToDiscard_thenTheCardIsRemovedFromThePlayersHand_andTheCardIsReplacedWithANewCard_andTheOriginalCardIsReturnedToTheDeck() {
		PlayerInterface player = new Human(DECK, PLAYER_ID1, CREDITS);
		Card oldCard = player.getHand()[2];
		List<Card> cardsToDiscard = new ArrayList<>(1);
		cardsToDiscard.add(player.getHand()[2]);
		
		assertEquals(47, deck.size());
		
		player.discardCards(cardsToDiscard);
		
		assertEquals(47, deck.size());
		
		Card newCard = player.getHand()[2];
		assertNotEquals(oldCard, newCard);
	}
	
	@Test
	public void whenPlayerSelectsMultipleCardsToDiscard_thenTheCardsAreRemovedFromThePlayersHand_andTheyAreReplacedWithNewCards_andTheDiscardedCardsAreReturnedTotheDeck() {
		PlayerInterface player = new Human(DECK, PLAYER_ID1, CREDITS);
		
		Card oldCard1 = player.getHand()[2];
		Card oldCard2 = player.getHand()[1];
		Card oldCard3 = player.getHand()[4];
		
		assertEquals(47, deck.size());
		
		List<Card> cardsToDiscard = new ArrayList<>(1);
		
		cardsToDiscard.add(player.getHand()[2]);
		cardsToDiscard.add(player.getHand()[1]);
		cardsToDiscard.add(player.getHand()[4]);
		
		player.discardCards(cardsToDiscard);
		
		assertEquals(47, deck.size());
		
		Card newCard1 = player.getHand()[2];
		Card newCard2 = player.getHand()[1];
		Card newCard3 = player.getHand()[4];
		
		assertNotEquals(oldCard1, newCard1);
		assertNotEquals(oldCard2, newCard2);
		assertNotEquals(oldCard3, newCard3);
	}

}
