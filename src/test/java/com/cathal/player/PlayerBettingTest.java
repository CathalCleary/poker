package com.cathal.player;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;
import com.cathal.game.Game;

public class PlayerBettingTest {
	
	private static final DeckInterface DECK = Mockito.mock(Deck.class); 
	private static final String PLAYER_ID1 = "player1";
	private static final String PLAYER_ID2 = "player2";
	private static final Card[] HAND1 = new Card[5];
	private static final Card[] HAND2 = new Card[5];
	
	@Before
	public void setUpBeforeEachTest(){
		Game.potAmount = 0;
		Game.roundOver = false;
	}
	
	@BeforeClass
	public static void setUp(){
		HAND1[0] = new NumberedCard(Suit.Clubs, 1);
		HAND1[1] = new NumberedCard(Suit.Clubs, 2);
		HAND1[2] = new NumberedCard(Suit.Clubs, 3);
		HAND1[3] = new NumberedCard(Suit.Clubs, 7);
		HAND1[4] = new NumberedCard(Suit.Clubs, 9);
		
		HAND2[0] = new NumberedCard(Suit.Clubs, 10);
		HAND2[1] = new NumberedCard(Suit.Diamonds, 4);
		HAND2[2] = new NumberedCard(Suit.Diamonds, 5);
		HAND2[3] = new NumberedCard(Suit.Hearts, 9);
		HAND2[4] = new NumberedCard(Suit.Spades, 8);
		
		Mockito.when(DECK.deal()).thenReturn(HAND1).thenReturn(HAND2);
	}
	

	@Test
	public void whenPlayerBetsCredits_thenThePotIsIncreasedByThatAmount_andThePlayersStashIsDecreasedByThatAmount() {
		PlayerInterface player = new Human(DECK, PLAYER_ID1, 1000);
		assertEquals(1000, player.getCredits());
		
		player.bet(250);
		
		assertEquals(750, player.getCredits());
		
		assertEquals(250, Game.potAmount);
	}
	
	@Test
	public void whenPlayerOneBetsCredits_andPlayerTwoSeesCredits_thenPlayersCreditsAreDecreasedByThatAmount_andThePotIsIncreasedByThatAmount_andTheRoundIsOver() {
		PlayerInterface player1 = new Human(DECK, PLAYER_ID1, 1000);
		PlayerInterface player2 = new Human(DECK, PLAYER_ID2, 1000);
		assertFalse(Game.roundOver);
		assertEquals(1000, player1.getCredits());
		assertEquals(1000, player2.getCredits());
		
		player1.bet(250);
		assertEquals(250, Game.potAmount);
		assertEquals(750, player1.getCredits());
		player2.seeAndRaise(250, 0);
		assertEquals(500, Game.potAmount);
		assertEquals(750, player2.getCredits());
		assertFalse(player1.isFolded());
		assertFalse(player2.isFolded());
		assertTrue(Game.roundOver);
		
	}
	
	@Test
	public void whenPlayerOneBetsCredits_andPlayerTwoFolds_thenPlayersCreditsAreNotDecreased_andThePotIsNotIncreased_andTheRoundsOver() {
		PlayerInterface player1 = new Human(DECK, PLAYER_ID1, 1000);
		PlayerInterface player2 = new Human(DECK, PLAYER_ID2, 1000);
		assertFalse(Game.roundOver);
		assertEquals(1000, player1.getCredits());
		assertEquals(1000, player2.getCredits());
		
		player1.bet(250);
		assertEquals(250, Game.potAmount);
		assertEquals(750, player1.getCredits());
		player2.fold();
		assertTrue(player2.isFolded());
		assertEquals(250, Game.potAmount);
		assertEquals(1000, player2.getCredits());
		assertTrue(Game.roundOver);
		
	}
	
	@Test
	public void whenPlayerOneBetsCredits_andPlayerTwoSees_andPlayerTwoRaises_thenPlayersCreditsAreDecreasedByThatAmountCombined_andThePotIsIncreasedByThatAmountCombined_andRaiseIsIncreasedByAmountRaised_andTheRoundContinues() {
		PlayerInterface player1 = new Human(DECK, PLAYER_ID1, 1000);
		PlayerInterface player2 = new Human(DECK, PLAYER_ID2, 1000);
		assertFalse(Game.roundOver);
		assertEquals(1000, player1.getCredits());
		assertEquals(1000, player2.getCredits());
		
		player1.bet(250);
		assertEquals(250, Game.potAmount);
		assertEquals(750, player1.getCredits());
		player2.seeAndRaise(250, 250);
		assertEquals(750, Game.potAmount);
		assertEquals(500, player2.getCredits());
		assertEquals(250, Game.raisedAmount);
		assertFalse(Game.roundOver);
		
	}

}
