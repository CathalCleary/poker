package com.cathal.player;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;

public class ComputerJunkTest {

	@Test
	public void WhenComputerHasJunk_thenComputerFolds() {
		final DeckInterface DECK = Mockito.mock(Deck.class);
		final Card [] hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Diamonds, 1);
		hand[2] = new NumberedCard(Suit.Hearts,2);
		hand[3] = new NumberedCard(Suit.Diamonds, 3);
		hand[4] = new NumberedCard(Suit.Spades, 6);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
		
		ComputerInterface computer = new Computer(DECK, 1000);
		
		assertEquals("fold", computer.decideNextMove());
	}

}
