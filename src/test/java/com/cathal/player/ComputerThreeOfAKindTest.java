package com.cathal.player;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;

public class ComputerThreeOfAKindTest {
	
	private static Card [] hand;
	private static final DeckInterface DECK = Mockito.mock(Deck.class);
	
	@BeforeClass
	public static void setUp(){
		hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Diamonds, 1);
		hand[2] = new NumberedCard(Suit.Hearts,3);
		hand[3] = new NumberedCard(Suit.Diamonds, 3);
		hand[4] = new NumberedCard(Suit.Spades, 3);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
	}
	

	@Test
	public void whenComputerHasThreeOfAKind_andFundsAreGreaterThanSevenHundred_thenComputerRaises() {
		ComputerInterface computer = new Computer(DECK, 701);
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void whenComputerHasThreeOfAKind_andFundsAreEqualToSevenHundred_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 701);
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void whenComputerHasThreeOfAKind_andFundsAreLessThanSevenHundred_thenComputerSees(){
		ComputerInterface computer = new Computer(DECK, 699);
		assertEquals("see", computer.decideNextMove());
	}

}
