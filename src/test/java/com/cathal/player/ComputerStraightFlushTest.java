package com.cathal.player;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;

public class ComputerStraightFlushTest {

	private static final DeckInterface DECK = Mockito.mock(Deck.class);
	private static Card [] hand;
	
	@BeforeClass
	public static void setUp(){
		hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Clubs, 5);
		hand[2] = new NumberedCard(Suit.Clubs,6);
		hand[3] = new NumberedCard(Suit.Clubs, 7);
		hand[4] = new NumberedCard(Suit.Clubs, 8);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
	}
	
	@Test
	public void whenComputerHasAStraightFlush_andComputerHasFunds_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 1);
		
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void whenComputerHasAStraight_andComputerHasZeroFunds_thenComputerFolds(){
		ComputerInterface computer = new Computer(DECK, 0);
		
		assertEquals("fold", computer.decideNextMove());
	}
	

}
