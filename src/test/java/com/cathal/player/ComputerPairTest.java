package com.cathal.player;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;

public class ComputerPairTest {
	
	private static final DeckInterface DECK = Mockito.mock(Deck.class);
	private static Card [] hand;
	
	@BeforeClass
	public static void setUp(){
		hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Diamonds, 1);
		hand[2] = new NumberedCard(Suit.Hearts,1);
		hand[3] = new NumberedCard(Suit.Diamonds, 2);
		hand[4] = new NumberedCard(Suit.Spades, 3);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
	}

	@Test
	public void testWhenComputerHasAPair_andFundsAreGreaterThanNineHundred_thenComputerSees() {
		ComputerInterface computer = new Computer(DECK, 901);
		
		assertEquals("see", computer.decideNextMove());
	}
	
	@Test
	public void testWhenComputerHasAPair_andFundsIsEqualToNineHundred_thenComputerSees() {
		ComputerInterface computer = new Computer(DECK, 900);
		
		assertEquals("see", computer.decideNextMove());
	}
	
	@Test
	public void whenComputerHasAPair_andFundsAreLessThanNineHundred_thenComputerFolds(){
		ComputerInterface computer = new Computer(DECK, 899);
		
		assertEquals("fold", computer.decideNextMove());
	}

}
