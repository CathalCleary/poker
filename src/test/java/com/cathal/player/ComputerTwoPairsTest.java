package com.cathal.player;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;

public class ComputerTwoPairsTest {
	
	private static Card [] hand;
	private static final DeckInterface DECK = Mockito.mock(Deck.class);
	
	@BeforeClass
	public static void setUp(){
		hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Diamonds, 1);
		hand[2] = new NumberedCard(Suit.Hearts,1);
		hand[3] = new NumberedCard(Suit.Diamonds, 3);
		hand[4] = new NumberedCard(Suit.Spades, 3);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
	}
	
	@Test
	public void whenComputerHasTwoPairsTestAndFundsAreGreaterThanSevenHundred_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 701);
		
		assertEquals("raise", computer.decideNextMove());
		
	}
	
	@Test
	public void whenComputerHasTwoPairsTestAndFundsIsEqualToSevenHundred_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 700);
		
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void whenComputerHasTwoPairsTestAndFundsIsGreaterThanSixHundredAndLessThanSevenHundred_thenComputerSees(){
		ComputerInterface computer = new Computer(DECK, 699);
		
		assertEquals("see", computer.decideNextMove());
		
	}
	
	@Test
	public void whenComputerHasTwoPairsTestAndFundsIsEqualToSixHundredAndLessThanSevenHundred_thenComputerSees(){
		ComputerInterface computer = new Computer(DECK, 600);
		
		assertEquals("see", computer.decideNextMove());
		
	}
	
	@Test
	public void whenComputerHasTwoPairsTestAndFundsIsLessThanSixHundred_thenComputerSees(){
		ComputerInterface computer = new Computer(DECK, 600);
		
		assertEquals("see", computer.decideNextMove());
		
	}

}
