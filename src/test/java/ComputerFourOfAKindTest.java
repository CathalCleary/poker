import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.cathal.deck.Card;
import com.cathal.deck.Deck;
import com.cathal.deck.DeckInterface;
import com.cathal.deck.NumberedCard;
import com.cathal.deck.Suit;
import com.cathal.player.Computer;
import com.cathal.player.ComputerInterface;


public class ComputerFourOfAKindTest {

	private static Card [] hand;
	private static final DeckInterface DECK = Mockito.mock(Deck.class);
	
	@BeforeClass
	public static void setUp(){
		hand = new Card[5];
		
		hand[0] = new NumberedCard(Suit.Clubs, 4);
		hand[1] = new NumberedCard(Suit.Diamonds, 4);
		hand[2] = new NumberedCard(Suit.Hearts,4);
		hand[3] = new NumberedCard(Suit.Spades, 4);
		hand[4] = new NumberedCard(Suit.Clubs, 3);
		
		Mockito.when(DECK.deal()).thenReturn(hand);
	}
	
	@Test
	public void testWhenComputerHasFourOfAKind_andFundsAreGreaterThanThreeHundred_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 301);
		
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void testWhenComputerHasFourOfAKind_andFundsAreEqualToThreeHundred_thenComputerRaises(){
		ComputerInterface computer = new Computer(DECK, 300);
		
		assertEquals("raise", computer.decideNextMove());
	}
	
	@Test
	public void testWhenComputerHasFourOfAKind_andFundsAreLessThanThreeHundred_thenComputerSees(){
		ComputerInterface computer = new Computer(DECK, 299);
		
		assertEquals("see", computer.decideNextMove());
	}
}
